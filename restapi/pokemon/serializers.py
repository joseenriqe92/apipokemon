from rest_framework import serializers 
from pokemon.models import Pokemon, Pokemon_user

class PokemonSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Pokemon
        fields = ('id',
                  'abilities',
                  'capture_rate',
                  'color',
                  'flavor_text',
                  'height',
                  'moves',
                  'name',
                  'sprites',
                  'stats',
                  'types',
                  'weight')

class PokemonUserSerializer(serializers.ModelSerializer):
    specie = PokemonSerializer(read_only=True)
    class Meta:
        model = Pokemon_user
        fields = ('id','specie', 'user','nick_name','is_party_member')
