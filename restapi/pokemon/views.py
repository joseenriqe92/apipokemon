from django.shortcuts import render
from rest_framework.parsers import JSONParser
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from pokemon.serializers import PokemonSerializer
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from rest_framework.permissions import IsAuthenticated

from .models import Pokemon, Pokemon_user
from .serializers import PokemonSerializer, PokemonUserSerializer

class pokemonView(APIView):
    def get(self, request, pk=None):
        if pk:
            pokemon = get_object_or_404(Pokemon.objects.all(), pk=pk)
            serializer = PokemonSerializer(pokemon)
            return Response(serializer.data)
        getallpokemon = Pokemon.objects.all()
        return Response(getallpokemon)

    def post(self, request):
        pokemon_data = JSONParser().parse(request)
        serializer = PokemonSerializer(data=pokemon_data)
        if serializer.is_valid(raise_exception=True):
            pokemon_saved = serializer.save()
        return Response({"success": "save pokemon"})

class pokemonUser(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, pk=None):
        dataToken = request.META['HTTP_AUTHORIZATION']
        idUser = Token.objects.filter(key=dataToken.split()[1])
        pokemonUser = get_object_or_404(Pokemon_user.objects.select_related('specie'), user=idUser[0].user_id)
        serializer = PokemonUserSerializer(pokemonUser)
        return Response(serializer.data)

    def post(self, request):
        permission_classes = (IsAuthenticated,) 
        dataToken = request.META['HTTP_AUTHORIZATION']
        token = Token.objects.filter(key=dataToken.split()[1])
        print(token[0].user_id)
        saveUser = Pokemon_user()
        saveUser.user = User.objects.get(pk=token[0].user_id)
        saveUser.specie = Pokemon.objects.get(pk=request.data['specie'])
        saveUser.nick_name = request.data['nick_name']
        saveUser.is_party_member = request.data['is_party_member']
        saveUser.save()
        return Response({'Save pokemon own'})
    
    def put(self, request, pk):
        updatePokemonUser = Pokemon_user.objects.get(pk=pk)
        data = JSONParser().parse(request)
        serializer = PokemonUserSerializer(updatePokemonUser, data=data, partial= True)
        if serializer.is_valid(raise_exception=True):
            pokemonUpdate = serializer.save()
            return Response(serializer.data)
    
    def delete(self, request, pk):
        # Get object with this pk
        pokemonDelete = get_object_or_404(Pokemon_user.objects.all(), pk=pk)
        pokemonDelete.delete()
        return Response(status=204)