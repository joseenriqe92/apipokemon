from django.db import models
from django.contrib.postgres.fields import JSONField
from django.contrib.postgres.fields import ArrayField
from django.conf import settings
from django.contrib.auth.models import User

class Pokemon(models.Model):
    abilities = ArrayField(models.CharField(max_length=250), blank=False)
    capture_rate = models.IntegerField()
    color = models.CharField(max_length=50, blank = True, null = True)
    flavor_text = models.CharField("Flavor Text", max_length=255, blank=True, null=True)
    height =models.IntegerField()
    moves = ArrayField(models.CharField(max_length=250), blank=False)
    name = models.CharField(max_length=50, blank = True, null = True)
    sprites = JSONField()
    stats = JSONField()
    types = ArrayField(models.CharField(max_length=50), blank=False)
    weight =models.IntegerField()

class Pokemon_user(models.Model):
    specie  = models.ForeignKey(Pokemon, on_delete=models.CASCADE)
    user  = models.ForeignKey(User, on_delete=models.CASCADE)
    nick_name = models.CharField(max_length=128, unique=True)
    is_party_member = models.BooleanField(null=False)
