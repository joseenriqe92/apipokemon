from django.conf.urls import url 
from .views import pokemonView, pokemonUser
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token  # <-- Here

urlpatterns = [ 
    path('login', obtain_auth_token, name='api_token_auth'),
    path('pokemons', pokemonView.as_view()),
    path('pokemon/<int:pk>', pokemonView.as_view()),
    path('pokemons/own/', pokemonUser.as_view()),
    path('pokemons/own/<int:pk>', pokemonUser.as_view()),
]